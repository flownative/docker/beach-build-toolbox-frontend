# Prepare node jessie image for node and npm binaries
FROM node:8-jessie as node

FROM registry.gitlab.com/flownative/docker/base:1
MAINTAINER Robert Lemke <robert@flownative.com>

ENV PYTHON /usr/bin/python2.7

# This toolbox will be used as user "beach" by default:
RUN groupadd -r -g 1000 beach && useradd -s /bin/bash -r -g beach -G beach -p "*" -u 1000 beach
RUN mkdir -p /home/beach \
    && chown beach:beach /home/beach \
    && mkdir /build \
    && chown beach:beach /build

# Copy Node and npm binaries from node:8-jessie
COPY --from=node /usr/local/bin/node /usr/local/bin/node
COPY --from=node /usr/local/lib/node_modules /usr/local/lib/node_modules
RUN ln -s /usr/local/lib/node_modules/npm/bin/npm-cli.js /usr/local/bin/npm

# Install build essentials (including "make") and Yarn:
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list \
    && apt-get update \
    && apt-get install -y --no-install-recommends --no-install-suggests \
        build-essential \
        python2.7 \
        yarn \
    && ln -s /usr/bin/python2.7 /usr/bin/python \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /build

COPY root-files /

USER beach

ENTRYPOINT ["/entrypoint.sh"]
